﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	public static GameManager Instance = null;

	public PrefabsManager pfManager;
	public ItemManager itemsManager;
	public NavigationManager navManager;
	public StationManager stationManager;
	public HumansManager humansManager;
	public UnityEngine.UI.Text scoreText;
	public UnityEngine.UI.Text gameTimeText;


	private float copTimer;
	private float[] clientTimers;
	private float trainTimer;
	private int clientTimersCurrentIndex;
	private int highScore;

	private float gameTime;

	void Awake()
	{
		// First we check if there are any other instances conflicting
		if(Instance != null && Instance != this)
		{
			// If that is the case, we destroy other instances
			Destroy(gameObject);
		}

		// Here we save our singleton instance
		Instance = this;

		// Furthermore we make sure that we don't destroy between scenes (this is optional)
		DontDestroyOnLoad(gameObject);

		pfManager = GetComponent<PrefabsManager> ();
		itemsManager = GetComponent<ItemManager> ();
		navManager = GetComponent<NavigationManager> ();
		stationManager = GetComponent<StationManager> ();
		humansManager = GetComponent<HumansManager> ();
	}


	void Start()
	{
		CreateChateletStation ();
	}

	void CreateChateletStation()
	{
		//Timers
		copTimer = 5f;
		trainTimer = 3f;

		clientTimers = new float[10];
		for (int i = 0; i < clientTimers.Length; i++) 
		{
			clientTimers [i] = (Random.Range (10f, 16f));
		}
		clientTimersCurrentIndex = 0;

		highScore = 0;

		gameTime = 300f;

		//Add trains
		//
	}

	void SendNewTrain()
	{
		GameObject trainGO = Instantiate (pfManager.PFGetTrain ());
		Train train = trainGO.AddComponent<Train> ();
		train.SendTrain ();
	}

	void Update()
	{
		copTimer -= Time.deltaTime;
		trainTimer -= Time.deltaTime;
		clientTimers [clientTimersCurrentIndex] -= Time.deltaTime;
		gameTime -= Time.deltaTime;

		if (copTimer <= 0f) 
		{
			if (stationManager.GetPlatformWithoutCop () != null)
				humansManager.PopACop (pfManager.PFGetCop (), stationManager.GetPlatformWithoutCop ());
			copTimer = Random.Range (10f, 20f);
		}

		if (trainTimer <= 0f) 
		{
			SendNewTrain ();
			trainTimer = 35f;
		}

		if (gameTime >= 0) {
			gameTimeText.text = ((int)gameTime).ToString ();
		} else {
			MenuManager.TitleScreen ();
		}

		if (clientTimers [clientTimersCurrentIndex] <= 0f) 
		{
			clientTimers [clientTimersCurrentIndex] = Random.Range (10f, 16f);
			Platform clientPlatform = stationManager.GetRandomPlatform ();
			if (clientPlatform != null) 
			{
				humansManager.PopAClient (pfManager.PFGetRandomClient (), clientPlatform);
				if (clientTimersCurrentIndex % clientTimers.Length == 0)
					clientTimersCurrentIndex = 0;
				else
					clientTimersCurrentIndex++;
			}
		}

		if (Input.GetKeyUp (KeyCode.A)) 
		{
			TEST_RandomHumanShake ();
		}
			
	}

	public void TEST_RandomHumanShake()
	{
		Human human = humansManager.GetRandomHumanWithItem ();
		if (human != null) 
		{
			Debug.LogError ("TEST RANDOM SHAKE");

			human.GetShaked ();
		}
	}

	public void UpdateScore(int value)
	{
		highScore += value;
		Debug.LogError ("SCORE > " + highScore);
		scoreText.text = highScore.ToString ();
	}
}
