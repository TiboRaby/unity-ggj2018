﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationManager : MonoBehaviour 
{
	public List<Platform> platforms = new List<Platform> ();

	public Platform GetRandomPlatform()
	{
		return platforms [Random.Range (0, platforms.Count)];
	}

	public Platform GetPlatformWithoutCop()
	{
		Platform result = null;

		var available = platforms.FindAll ((x) => x.hasCop == false);

		result = available [Random.Range (0, available.Count)];

		return result;
	}
}
