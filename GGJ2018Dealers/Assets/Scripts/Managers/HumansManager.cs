﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumansManager : MonoBehaviour 
{
	public List<GameObject> humansPool = new List<GameObject> ();

	private List<Human> humansList = new List<Human>();

	public void AddHumanToGame(Human human)
	{
		humansPool.Add (human.gameObject);
		humansList.Add (human);
	}

	public void RemoveHumanFromGame(Human human)
	{
		humansPool.Remove (human.gameObject);
		humansList.Remove (human);
		StartCoroutine (FadeAndDestroyHuman (human.gameObject));
	}

	public void PopACop(GameObject prefab, Platform platform)
	{
		GameObject copGO = Instantiate (prefab, platform.transform, true);
		Cop cop = copGO.AddComponent<Cop> ();
		cop.Init (platform);
		cop.Spawn ();
		platform.hasCop = true;
	}

	public void PopAClient(GameObject prefab,Platform platform)
	{
		GameObject clientGO = Instantiate (prefab, platform.transform, true);
		Client client = clientGO.AddComponent<Client> ();
		client.Init (platform);
		client.Spawn ();
	}

	public Human GetRandomHumanWithItem()
	{
		var list = this.humansList.FindAll ((x) => x is Human && x.itemToGive > Item.ItemType.None);
		if (list.Count > 0)
			return list [Random.Range (0, list.Count)];
		else
			return null;
	}

	private IEnumerator FadeAndDestroyHuman(GameObject itemGO)
	{
		yield return new WaitForSeconds (1.5f);
		Destroy (itemGO);
	}
}
