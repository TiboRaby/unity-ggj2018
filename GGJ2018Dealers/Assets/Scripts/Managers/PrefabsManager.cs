﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabsManager : MonoBehaviour 
{
	//Prefabs Manager
	[Header("Station")]
	public GameObject chateletStation;

	[Header("Players")]
	public GameObject[] players;

	[Header("Humans")]
	public GameObject[] clients;
	public GameObject cop;
	public GameObject crowdPeon;

	[Header("Items")]
	public GameObject[] items;

	[Header("Trains")]
	public GameObject train;

	 

	public GameObject PFGetMetroStation()
	{
		return chateletStation;
	}

	public GameObject PFGetTrain()
	{
		return train;
	}

	public GameObject PFGetPlayer(int index)
	{
		return players [index];
	}

	public GameObject PFGetRandomClient()
	{
		return this.clients [Random.Range (0, clients.Length)];
	}

	public GameObject PFGetCop()
	{
		return cop;
	}

	public GameObject PFGetPeon()
	{
		return crowdPeon;
	}

	public GameObject PFGetItem(int index)
	{
		return items [index];
	}

}
