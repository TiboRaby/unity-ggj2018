﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour 
{
	public List<GameObject> itemsPool = new List<GameObject> ();

	private List<Item> itemList = new List<Item>();

	public void AddItemToGame(Item item)
	{
		itemsPool.Add (item.gameObject);
		itemList.Add (item);
	}

	public void RemoveItemFromGame(Item item)
	{
		itemsPool.Remove (item.gameObject);
		itemList.Remove (item);
		item.canBeGrabbed = false;
		StartCoroutine (FadeAndDestroyItem (item.gameObject));
	}

	private IEnumerator FadeAndDestroyItem(GameObject itemGO)
	{
		yield return new WaitForSeconds (1f);
		Destroy (itemGO);
	}
}
