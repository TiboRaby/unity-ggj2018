﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationManager : MonoBehaviour 
{
//	public void MoveToDestination(GameObject source, SpawnPoint destination)
//	{
//		StartCoroutine (MoveMyObject (source.transform, destination.transform));
//	}

	public void MoveToDest(Human source, SpawnPoint destination, HumanBody bodyRef)
	{
		StartCoroutine (MoveMyHuman (source, destination.transform, bodyRef));
	}

	public void MoveTrain(Transform origin, Transform dest, Train train, bool leave=false)
	{
		if (leave == false)
			StartCoroutine (MoveMyTrain (origin, dest, train));
		else 
		{
			if (train.platformOfChoice.ID.Contains ("4") || train.platformOfChoice.ID.Contains ("2"))
				StartCoroutine (TrainLeave (origin, Vector3.left, train));
			else
				StartCoroutine (TrainLeave (origin, Vector3.right, train));
		}
			
	}

	IEnumerator MoveMyHuman(Human source, Transform destination, HumanBody bodyRef)
	{
		float speed = Random.Range (1.3f, 2f);
		if (source is Cop) 
		{
			if ((source as Cop).targetItem != null)
				speed *= 2f;
			else
				speed = speed - 0.3f;
		}

		float deltaToTarget = source.GetType()==typeof(Cop) || source.GetType()==typeof(Client) ? 0.3f : 4f;

		//HumanBody bodyRef = source.gameObject.GetComponent<HumanBody> ();

		while (Vector3.Distance (source.transform.position, destination.position) > deltaToTarget) 
		{
			float step = speed * Time.deltaTime;

			//bodyRef.Move (destination.position - source.transform.position);

			source.transform.position = Vector3.MoveTowards (source.transform.position, destination.position, step);
			source.transform.forward = destination.position;
			yield return null;
		}
		source.HasReachedDestination ();
		yield break;
	}

	IEnumerator MoveMyTrain(Transform origin, Transform destination, Train train)
	{
		float speed = Random.Range (1.3f, 2f);

		while (Vector3.Distance (origin.position, destination.position) > 0.2f) 
		{
			float step = speed * Time.deltaTime;
			origin.position = Vector3.MoveTowards (origin.position, destination.position, step);
			yield return null;
		}
		train.HasArrived ();
		yield break;
	}

	IEnumerator TrainLeave(Transform origin, Vector3 direction, Train train)
	{
		float timeBeforeDelete = 4f;

		while (timeBeforeDelete > 0f) 
		{
			origin.transform.position += (direction / timeBeforeDelete * 0.2f);
			timeBeforeDelete -= Time.deltaTime;
			yield return null;
		}
		Destroy (train.gameObject);
		yield break;
	}
}
