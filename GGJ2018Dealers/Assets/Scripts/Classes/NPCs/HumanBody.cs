﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class HumanBody : SelectableItems {

	[SerializeField] float m_SpeedMax = 5f;
    [SerializeField] float m_Acceleration = 4000f;

    Rigidbody m_Rigidbody;
	Animator m_Animator;
	const float k_Half = 0.5f;
	float m_ForwardAmount;

	Vector3 m_MoveDirection = Vector3.zero;

	bool m_IgnoreInput = false;
	public bool IgnoreInput{ get; set;}

	public GameObject bubble;
	public Sprite[] bubblesIcons = new Sprite[4];

	void Start()
	{
		m_Animator = GetComponent<Animator>();
		m_Rigidbody = GetComponent<Rigidbody>();

		m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
	}


	public void Move(Vector3 moveInput)
	{
		if(this.m_IgnoreInput)
			return;

        moveInput.y = 0;
        m_ForwardAmount = moveInput.sqrMagnitude;
        
		if(m_ForwardAmount > 0.05f)
		{
			m_MoveDirection = moveInput.normalized;

            Vector3 currentAcceleration = m_MoveDirection * this.m_Acceleration * Time.deltaTime;
            m_Rigidbody.AddForce(currentAcceleration);
            if(m_Rigidbody.velocity.magnitude > m_SpeedMax)
            {
                m_Rigidbody.velocity = m_Rigidbody.velocity.normalized * m_SpeedMax;
            }

        }

        if(m_MoveDirection != Vector3.zero)
            RotateToward(m_MoveDirection);

        UpdateAnimator();
    }


	public void UpdateIcon(Item.ItemType itemType)
	{
		Sprite result = null;
		switch (itemType) 
		{
		case Item.ItemType.Handbag:
			result = bubblesIcons [0];
			break;
		case Item.ItemType.Drugs:
			result = bubblesIcons [1];
			break;
		case Item.ItemType.Smartphone:
			result = bubblesIcons [2];
			break;
		case Item.ItemType.Syringe:
			result = bubblesIcons [3];
			break;
		}
		if (result != null)
			bubble.GetComponentInChildren<SpriteRenderer> ().sprite = result;
	}

	public void UpdateAnimator()
	{
		// update the animator parameters
		m_Animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
	}

	void RotateToward(Vector3 moveDirection)
	{
        if(moveDirection != Vector3.zero)
        {
            transform.forward = moveDirection.normalized;
        }
	}

	public void ChangeForward(float value)
	{
		this.m_ForwardAmount = value;
	}

	public void ChangeSpeed(float value)
	{
		this.m_SpeedMax = value;
	}
}
