﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Client : Human 
{
	//The BUYER spawns from the Exit point to an NPC point on a platform
	//He waits for a limited time once he reaches the NPC point
	//If no NPC point available or expected time reached, he leaves
	public override void Init(Platform platform)
	{
		//Buyer the player gives an item to
		this.ID = string.Format ("Consumer_{0}", Random.Range (0, 200));
		this.lifeTime = Random.Range (10f, 20f);
		this.expectedItem = (Item.ItemType)Random.Range (1, System.Enum.GetValues (typeof(Item.ItemType)).Length);
		this.itemToGive = Item.ItemType.None;
		this.prefab = GameManager.Instance.pfManager.PFGetRandomClient ();
		this.currentPlatform = platform;
		this.startingPosition = this.currentPlatform.GetExitSpawnPoint ();
		this.destination = this.currentPlatform.GetAvailableNPCPoint ();
		//this.bodyComponent.bubble.SetActive (false);
	}

	public override void Spawn()
	{
		base.Spawn ();
		this.bodyComponent.ChangeSpeed(60f);

//		this.transform.position = startingPosition.transform.position;
//		this.gameObject.SetActive (true);
//		this.gameObject.name = this.ID;
//
//		if (this.destination != null) 
//		{
//			base.MoveToDestination ();
//		}
//		else
//			Debug.LogError (string.Format ("{0} can't go to NULL destination!",this.ID)); 
	}

	public override void InteractWithPlayer(Player player)
	{
		base.InteractWithPlayer (player);
		ReceiveItem (player);
	}

	public override void HasReachedDestination ()
	{
		Debug.Log (this.ID + " has reached its destination");
		this.isMoving = false;

		//this.bodyComponent.Move (Vector3.zero);
		this.bodyComponent.ChangeForward (0f);
		this.bodyComponent.UpdateAnimator ();
		StartCoroutine (WaitForGoods (lifeTime));
	}

	//The buyer waits for a limited time then goes to the exit
	private IEnumerator WaitForGoods(float delay)
	{
		this.bodyComponent.UpdateIcon (this.expectedItem);
		this.bodyComponent.bubble.SetActive (true);
		while (delay > 0f) 
		{
			delay -= Time.deltaTime;
			yield return null;
		}
		this.destination.isBusy = false;
		this.destination = this.currentPlatform.GetExitSpawnPoint ();
		Debug.LogError (this.ID + "is TIRED of waiting! ...");
		base.MoveToDestination ();
	}

	public override float GetDeltaTarget()
	{
		return 0.4f;
	}

	//The buyer receives an item from the player
	private void ReceiveItem(Player player)
	{
		if (this.expectedItem != Item.ItemType.None) 
		{
			//Receive item from player, check if the right type of item
			if (this.expectedItem == player.currentItem.type) 
			{
				//CONTENT
				this.currentItem = player.currentItem;
				this.currentItem.transform.SetParent (this.transform);
				this.destination.isBusy = false;
				base.DeleteHuman ();
				GameManager.Instance.UpdateScore (100 * (Random.value > 0.5f ? 2 : 1));
				player.currentItem = null;
				//TODO delete item from player, update the score
			} 
			else 
			{
				//PAS CONTENT
				this.currentItem = player.currentItem;
				this.currentItem.transform.SetParent (this.transform);
				this.destination.isBusy = false;
				base.DeleteHuman ();
				GameManager.Instance.UpdateScore (10 * (Random.value > 0.5f ? 2 : 1));
				player.currentItem = null;
				//TODO delete item from player, update the score
			}
		}
	}


}
