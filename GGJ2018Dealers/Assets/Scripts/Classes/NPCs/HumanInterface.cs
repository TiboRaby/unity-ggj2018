﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface HumanInterface
{
	void InteractWithPlayer (Player player);
	void MoveToDestination ();
}
