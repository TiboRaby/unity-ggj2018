﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientToPLayerDetection : MonoBehaviour {

	public string ItemTag = "Player";

	private Client clientComponent;
	private Client ClientComponent
	{
		get{
			if(clientComponent == null)
				clientComponent = this.GetComponentInParent<Client>();
			return clientComponent;
		}
		set{}
	}

//	public Player currentlyAvailablePlayer = null;
//	public List<Player> availableItems;

//	private List<ItemDetectionListener<Player>> m_Listeners;


	//------------------- Listener -------------------//
//	public void RegisterListener(ItemDetectionListener<Player> newListener)
//	{
//		if(m_Listeners == null)
//			m_Listeners = new List<ItemDetectionListener<Player>>();
//		m_Listeners.Add(newListener);
//	}
//	public void UnregisterListener(ItemDetectionListener<Player> newListener)
//	{
//		if(m_Listeners == null)
//			m_Listeners = new List<ItemDetectionListener<Player>>();
//		m_Listeners.Remove(newListener);
//	}
//
//	private void SendNewItemToListeners(Player newItem)
//	{
//		if(this.m_Listeners == null)
//			return;
//
//		List<int> deleteIndexList = new List<int>();
//
//		for(int i = 0; i < this.m_Listeners.Count; i++)
//		{
//			if(this.m_Listeners[i] == null)
//			{
//				deleteIndexList.Add(i);
//			}
//			else {
//				this.m_Listeners[i].NewAvailableItem(newItem);
//			}
//		}
//
//		// Remove missing items stuff
//		for(int i = 0; i < deleteIndexList.Count; i++)
//		{
//			this.m_Listeners.RemoveAt(i);
//		}
//	}
//
//	private void SendNoItemCurrentlyAvailableToListener()
//	{
//		if(this.m_Listeners == null)
//			return;
//
//		List<int> deleteIndexList = new List<int>();
//
//		for(int i = 0; i < this.m_Listeners.Count; i++)
//		{
//			if(this.m_Listeners[i] == null)
//			{
//				deleteIndexList.Add(i);
//			}
//			else {
//				this.m_Listeners[i].NoItemCurrentlyAvailable();
//			}
//		}
//
//		// Remove missing items stuff
//		for(int i = 0; i < deleteIndexList.Count; i++)
//		{
//			this.m_Listeners.RemoveAt(i);
//		}
//	}
	//------------------- Listener -------------------//

	// Update is called once per frame
//	void Update () {
//		ProcessAvailableItems();
//	}
//
//	void ProcessAvailableItems()
//	{
//		if(availableItems != null && availableItems.Count > 0)
//		{
//			KeyValuePair<int, float> closestItem = new KeyValuePair<int, float>(-1, Mathf.Infinity);
//			if(currentlyAvailablePlayer != null)
//				closestItem = new KeyValuePair<int, float>(-1,Vector3.Distance( transform.position, currentlyAvailablePlayer.transform.position)); // We don't care for object closer than this
//
//			List<int> deleteIndexList = new List<int>();
//
//			// TODO: iterate on available item list, check the closest one and displayActionPrompt on it
//			for(int i = 0; i < this.availableItems.Count; i++)
//			{
//				if(this.availableItems[i] == null)
//				{
//					deleteIndexList.Add(i);
//				}
//				else {
//					float distanceToObject = Vector3.Distance( transform.position, this.availableItems[i].transform.position);
//					if(closestItem.Value > distanceToObject)
//					{
//						closestItem = new KeyValuePair<int, float>(i, distanceToObject);
//					}
//				}
//			}
//
//			// Remove missing items stuff
//			for(int i = 0; i < deleteIndexList.Count; i++)
//			{
//				this.availableItems.RemoveAt(i);
//			}
//
//			// Do We have something usefull ?
//			SetCurrentlyAvailableItem(closestItem.Key);
//		}
//	}
//
//	void SetCurrentlyAvailableItem(int itemIndex)
//	{
//		// Do We have something usefull ?
//		if(itemIndex != -1)
//		{
//			// ACTUAL ACTION UP IN HERE
//			Player newItem = this.availableItems[itemIndex];
//			if(currentlyAvailablePlayer != null)
//			{
//				//currentlyAvailablePlayer.HideAction();
//			}
//
//			currentlyAvailablePlayer = newItem;
//			//currentlyAvailablePlayer.DisplayAction();
//			SendNewItemToListeners(currentlyAvailablePlayer);
//		}
//	}
//
//	void ResetCurrentlyAvailable()
//	{
//		if(currentlyAvailablePlayer != null)
//		{
//			//currentlyAvailablePlayer.HideAction();
//			currentlyAvailablePlayer = null;
//			SendNoItemCurrentlyAvailableToListener();
//		}
//	}


	//------------------- Trigger -------------------//
	void OnTriggerEnter(Collider other) {
		if(other.CompareTag(ItemTag))
		{
			Player item = other.gameObject.GetComponent<Player>();
			if(item != null)
			{
				ClientComponent.InteractWithPlayer(item);
//				// TODO Should we check something on item ?
//				if(!availableItems.Contains(item))
//				{
//					availableItems.Add(item);
//				}
			}
		}
	}

//	void OnTriggerExit(Collider other) {
//		if(other.CompareTag(ItemTag))
//		{
//			Player item = other.gameObject.GetComponent<Player>();
//			if(item != null)
//			{
//				if(this.currentlyAvailablePlayer == item)
//					ResetCurrentlyAvailable();
//
//				// TODO Should we check something on item ?
//				if(availableItems.Contains(item))
//				{
//					availableItems.Remove(item);
//				}
//			}
//		}
//	}
}
