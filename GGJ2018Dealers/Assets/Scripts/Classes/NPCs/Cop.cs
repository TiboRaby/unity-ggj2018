﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cop : Human 
{
	//The COP spawns from the Exit point and finds an item on the ground to destroy
	//If a player is in his collision radius WITH an item on him or on the ground
	//the COP immobilizes the player for x-seconds, then disappear
	//If there's no player in the radius the cop goes to destroy the items on the ground one by one
	public Item targetItem;

	public override void Init(Platform platform)
	{
		this.ID = string.Format ("Cop_{0}", Random.Range (0, 200));
		this.lifeTime = Mathf.Infinity;
		this.expectedItem = Item.ItemType.None;
		this.itemToGive = Item.ItemType.None;
		this.prefab = GameManager.Instance.pfManager.PFGetCop ();
		this.currentPlatform = platform;
		this.startingPosition = this.currentPlatform.GetExitSpawnPoint ();
		this.destination = this.currentPlatform.GetCopPoint ();

		//get item on the ground
		//this.bodyComponent.bubble.SetActive(false);
		Debug.Log (this.ID + " initialized / platform:" + currentPlatform.ID + " / start:" + startingPosition.ID);
	}

	public override void Spawn()
	{
		base.Spawn ();
//		this.transform.position = startingPosition.transform.position;
//		this.gameObject.SetActive (true);
//		this.gameObject.name = this.ID;
//
//		if (this.destination != null) 
//		{
//			base.MoveToDestination ();
//		}
//		else
//			Debug.LogError (string.Format ("{0} can't go to NULL destination!",this.ID)); 
		this.bodyComponent.ChangeSpeed(70f);
	}

	public override void InteractWithPlayer(Player player)
	{
		base.InteractWithPlayer (player);
	}

	public override void HasReachedDestination ()
	{
		if (this.destination.typeOfSpawn == SpawnPoint.SpawnType.CopPoint) 
		{
			Debug.Log (this.ID + " has reached a Cop Point!");

			this.isMoving = false;
			//this.bodyComponent.Move (Vector3.zero);
			this.bodyComponent.ChangeForward (0f);
			this.bodyComponent.UpdateAnimator ();
			StartCoroutine (FindAnItemToDestroy (3f));
		} 
		else if (this.destination.typeOfSpawn == SpawnPoint.SpawnType.GroundItem) 
		{
			
			this.isMoving = false;
			//this.bodyComponent.Move (Vector3.zero);
			this.bodyComponent.ChangeForward (0f);
			this.bodyComponent.UpdateAnimator ();
			Debug.Log (this.ID + " has reached a Item to Destroy it!");
			DestroyItem (targetItem);
		}
	}

	private void DestroyItem(Item itemTodestroy)
	{
		//TODO animation of destroy
		this.currentPlatform.RemoveItemFromGround(itemTodestroy);
		GameManager.Instance.itemsManager.RemoveItemFromGame (itemTodestroy);
		targetItem = null;
		StartCoroutine (FindAnItemToDestroy (1f));
	}

	public override float GetDeltaTarget()
	{
		return 0.4f;
	}

	private IEnumerator FindAnItemToDestroy(float delay)
	{
		yield return new WaitForSeconds (delay);

		if (targetItem == null) 
		{
			this.targetItem = this.currentPlatform.GetItemOnTheGround ();
			if (this.targetItem != null) 
			{
				this.destination = this.targetItem.gameObject.AddComponent<SpawnPoint> ();
				this.destination.typeOfSpawn = SpawnPoint.SpawnType.GroundItem;
			}
			else {
				this.currentPlatform.hasCop = false;
				Debug.LogError (this.ID + " hasn't found any items! going back to Exit");
			}
		}
		if (targetItem == null)
			this.destination = this.currentPlatform.GetExitSpawnPoint ();

		base.MoveToDestination ();
		Debug.Log (this.ID + " is moving towards " + this.destination.ID);
	}


}
