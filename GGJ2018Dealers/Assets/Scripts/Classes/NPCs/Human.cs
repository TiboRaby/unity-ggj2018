﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Human : SelectableItems, HumanInterface
{
	public string ID;
	public float lifeTime;
	public SpawnPoint startingPosition;
	public Item.ItemType expectedItem;
	public Item.ItemType itemToGive;
	public GameObject prefab;
	public Platform currentPlatform;
	public SpawnPoint destination;
	public Item currentItem;
    
	public HumanBody bodyComponent;
	public bool isMoving = false;

	void Awake()
	{
		this.bodyComponent = this.GetComponent<HumanBody> ();
	}
        
	public virtual void Init(Platform platform)
	{
		this.ID = "Peon_" + Random.Range (0, 1000);
		this.lifeTime = Mathf.Infinity;
		this.expectedItem = Item.ItemType.None;
		if (Random.value > 0.5f)
			this.itemToGive = (Item.ItemType)Random.Range (1, System.Enum.GetValues (typeof(Item.ItemType)).Length);
		this.prefab = GameManager.Instance.pfManager.PFGetPeon ();
		this.currentPlatform = platform;
		this.startingPosition = this.currentPlatform.GetWagonStartingPoint ();
		this.destination = this.currentPlatform.GetExitSpawnPoint ();
		if (this.itemToGive > Item.ItemType.None) 
		{
			GameObject newItem = Instantiate (GameManager.Instance.pfManager.PFGetItem ((int)this.itemToGive - 1), this.transform);
			Item item = newItem.AddComponent<Item> ();
			item.ID = this.itemToGive.ToString () + "_" + Random.Range (0, 200);
			GameManager.Instance.itemsManager.AddItemToGame (item);
			//Hide the item
			currentItem = item;
			newItem.SetActive (false);
		}
		//this.bodyComponent.bubble.gameObject.SetActive (false);

//		Debug.Log (this.ID + " initialized / platform:" + currentPlatform.ID + " / start:" + startingPosition.ID + " / dest:" + destination.ID);
	}

	public virtual void Spawn()
	{
		this.transform.position = startingPosition.transform.position;
		this.gameObject.SetActive (true);
		this.gameObject.name = this.ID;

		GameManager.Instance.humansManager.AddHumanToGame (this);

		this.bodyComponent.bubble.gameObject.SetActive (false);

		if (this.destination != null) 
		{
			//if (this.destination.typeOfSpawn == SpawnPoint.SpawnType.Exit)
			this.isMoving = true;
			//MoveToDestination ();
		} 
		else
			Debug.LogError (this.ID + " has NULL destination !!!");
//		Debug.Log (this.ID + " spawned");
	}

	public virtual void InteractWithPlayer (Player player)
	{
		Debug.Log (this.ID + " interacting with " + player.ID);
	}

	public virtual void HasReachedDestination()
	{
		if (this.destination.typeOfSpawn == SpawnPoint.SpawnType.Exit)
			DeleteHuman ();
	}

	public void MoveToDestination ()
	{
		if (this.bodyComponent == null) {
			this.bodyComponent = this.GetComponent<HumanBody> ();
			if (this.bodyComponent == null)
				Debug.LogError ("Body is null !! " + this.ID);
		} else {
			this.isMoving = true;
			this.bodyComponent.Move ((this.destination.transform.position - this.transform.position).normalized);
		}
	}

	public void DeleteHuman()
	{
		if (this.currentItem != null) 
		{
			GameManager.Instance.itemsManager.RemoveItemFromGame (this.currentItem);
		}
		GameManager.Instance.humansManager.RemoveHumanFromGame (this);
	}

	public void GetShaked()
	{
		if (this.currentItem != null) 
		{
			Debug.Log (this.ID+" ITEM SHAKED TO POP >> " + this.currentItem.ID);

			this.currentPlatform.AddItemOnGround (this.currentItem);
			this.currentItem.transform.SetParent (this.currentPlatform.transform);
			//this.currentItem.gameObject.SetActive (false);
			this.currentItem = null;
		}
	}

	public virtual float GetDeltaTarget()
	{
		return 4f;
	}

	void FixedUpdate()
	{
		if (this.isMoving) {
			
			float deltaToTarget = this.GetDeltaTarget ();
			if (Vector3.Distance (transform.position, this.destination.transform.position) < deltaToTarget) {
				if (this.GetType () == typeof(Client))
					(this as Client).HasReachedDestination ();
				else if (this.GetType () == typeof(Cop))
					(this as Cop).HasReachedDestination ();
				else
					HasReachedDestination ();
			} else {
				MoveToDestination ();
			}
		} else {
//			this.bodyComponent.ChangeForward (0f);
//			this.bodyComponent.UpdateAnimator ();
			this.bodyComponent.Move (Vector3.zero);
		}
		
	}
}
