﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour 
{
	public string ID;

	public Item currentItem;


	public void ItemInReach(Item item)
	{
		Debug.Log (string.Format ("{0} is reachable by {1}", item.type, this.ID));
	}

	public void GrabItem()
	{
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void ShakeHuman(Human human)
	{
		human.GetShaked ();
	}
}
