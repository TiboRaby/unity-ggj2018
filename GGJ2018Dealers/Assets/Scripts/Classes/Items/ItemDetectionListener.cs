﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ItemDetectionListener<T> 
	where T : class
{
	void NewAvailableItem(T newItem);
	void NoItemCurrentlyAvailable(T oldItem);
}

