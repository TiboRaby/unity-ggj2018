﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableItems : MonoBehaviour
{
    public float normalOutlineValue = 0.0f;
    public float selectedOutlineValue = 0.08f;

    private Renderer renderComponent = null;
    private Renderer RenderComponent
    {
        get
        {
            if (renderComponent == null)
                renderComponent = this.GetComponentInChildren<Renderer>();

            return renderComponent;
        }
        set { }
    }

    // Use this for initialization
    public void Select () {
        RenderComponent.material.SetFloat("_OutlineWidth", this.selectedOutlineValue);
    }
	
	// Update is called once per frame
	public void Deselect () {
        RenderComponent.material.SetFloat("_OutlineWidth", this.normalOutlineValue);
    }
}
