﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : SelectableItems
{
	public enum ItemType
	{
		None,
		Drugs,
		Syringe,
		Handbag,
		Smartphone
	}
	public ItemType type;

	public SpawnPoint spawnPoint;

	public bool canBeGrabbed;
    

	public string ID;

	void OnTriggerEnter(Collider other)
	{
		//If a player reaches my collider
		if (other.CompareTag (GameTexts.playerTag))
			other.GetComponent<Player> ().ItemInReach (this);
	}

	//Spawn an item from the crowd
	public void Create(ItemType newType, Platform platform)
	{
		Debug.Log (string.Format ("{0} created on {1}", this.type, platform.ID));
	}
}
