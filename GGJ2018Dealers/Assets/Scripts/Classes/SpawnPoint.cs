﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour 
{
	public string ID;
	public bool isBusy = false;

	public enum SpawnType
	{
		Wagon,
		NPC,
		Exit,
		CopPoint,
		GroundItem,
		TrainStart
	}
	public SpawnType typeOfSpawn;
}
