﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Train : MonoBehaviour 
{
	public GameObject[] crowd;
	public Platform platformOfChoice;
	public Transform targetOnPlatform;

	public void SendTrain()
	{
		platformOfChoice = GameManager.Instance.stationManager.GetRandomPlatform ();
		gameObject.name = "Train_"+platformOfChoice.ID;
		transform.position = platformOfChoice.GetTrainStartPoint ().transform.position;	
		transform.rotation = platformOfChoice.GetTrainStartPoint ().transform.rotation;
		targetOnPlatform = platformOfChoice.GetWagonStartingPoint ().transform;


		Debug.Log ("Send new train! " + gameObject.name);

		GameManager.Instance.navManager.MoveTrain (this.transform, targetOnPlatform, this);
	}

	public void HasArrived()
	{
		CreateCrowd ();
	}

	void MakeTrainLeave()
	{
		GameManager.Instance.navManager.MoveTrain (this.transform, this.transform, this, true);
	}

	public void CreateCrowd()
	{
		crowd = new GameObject[10];

		for (int i = 0; i < crowd.Length; i++) 
		{
			crowd [i] = Instantiate (GameManager.Instance.pfManager.PFGetPeon ());
			crowd [i].SetActive (false);
			Human peon = crowd [i].AddComponent<Human> ();
			peon.Init (platformOfChoice);
		}
		Debug.Log ("Crowd created on " + platformOfChoice.ID + ", Spawning now...");
		StartCoroutine (ActivateCrowd (0));
	}

	IEnumerator ActivateCrowd(int index)
	{
		while (index < crowd.Length) 
		{
			crowd [index].GetComponent<Human> ().Spawn ();
			index += 1;
			yield return new WaitForSeconds (1f);
		}
		Debug.Log ("Crowd activated!...");
		MakeTrainLeave ();
		//CreateCrowd ();
		yield break;
	}

	void Update()
	{
	}

}
