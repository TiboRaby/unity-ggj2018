﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour 
{
	public string ID;
	public bool isBusy;
	public bool hasCop;
	public List<SpawnPoint> mySpawnPoints = new List<SpawnPoint>();
	public List<Item> itemsOnGround = new List<Item>();

	//Return the Exit point of this platform
	public SpawnPoint GetExitSpawnPoint()
	{
		if (mySpawnPoints.Find ((x) => x.typeOfSpawn == SpawnPoint.SpawnType.Exit) != null)
			return mySpawnPoints.Find ((x) => x.typeOfSpawn == SpawnPoint.SpawnType.Exit);
		else
			return null;
	}

	//Return the wagon starting point
	public SpawnPoint GetWagonStartingPoint()
	{
		return mySpawnPoints.Find ((x) => x.typeOfSpawn == SpawnPoint.SpawnType.Wagon);
	}

	//Return the train start point
	public SpawnPoint GetTrainStartPoint()
	{
		return mySpawnPoints.Find ((x) => x.typeOfSpawn == SpawnPoint.SpawnType.TrainStart);
	}

	//Return a random NPC waiting point on this platform
	public SpawnPoint GetAvailableNPCPoint()
	{
		List<SpawnPoint> availablePoints = mySpawnPoints.FindAll ((x) => x.typeOfSpawn == SpawnPoint.SpawnType.NPC && x.isBusy == false);
		SpawnPoint result = null;
		if (availablePoints.Count > 0) 
		{
			result = availablePoints [Random.Range (0, availablePoints.Count)];
			result.isBusy = true;
		}

		return result;
	}

	//Return a point on the ground for the cops to go to
	public SpawnPoint GetCopPoint()
	{
		return mySpawnPoints.Find ((x) => x.typeOfSpawn == SpawnPoint.SpawnType.CopPoint);
	}

	public Item GetItemOnTheGround()
	{
		if (this.itemsOnGround.Count > 0)
			return this.itemsOnGround [Random.Range (0, this.itemsOnGround.Count)];
		else
			return null;
	}

	//Add item on ground
	public void AddItemOnGround(Item refItem)
	{
		this.itemsOnGround.Add (refItem);
	}

	//Remove item on ground list
	public void RemoveItemFromGround(Item refItem)
	{
		if (this.itemsOnGround.Contains (refItem))
			this.itemsOnGround.Remove (refItem);
	}
}
