﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HumanBody))]
public class PlayerController : MonoBehaviour, ItemDetectionListener<Item>, ItemDetectionListener<Client>
{

	public string m_HorizontalAxisName = "Horizontal";
	public string m_VerticalAxisName = "Vertical";

	public string m_ActionInputKey = "Fire1"; // Joystick1Button0 => A button on windows

	HumanBody m_HumanBody;
	ItemDetection m_ItemDetection;
	PlayerToClientDetection m_ClientDetection;
    Player m_PlayerComponent;

	public Item m_GrabbedItem = null;
	public Vector3 throwingVector = Vector3.forward;

	public GameObject m_ActionBillboard;
	public GameObject m_GrabbingPoint;

	// Use this for initialization
	void Start () {
		m_HumanBody = GetComponent<HumanBody>();
		m_ItemDetection = GetComponentInChildren<ItemDetection>();
        m_ClientDetection = GetComponentInChildren<PlayerToClientDetection>();
        m_PlayerComponent = GetComponentInChildren<Player>();


        if (m_ActionBillboard != null)
			m_ActionBillboard.SetActive(false);

		m_ItemDetection.RegisterListener(this);
		m_ClientDetection.RegisterListener(this);
    }
	
	private void Update()
	{
		// Handle action event
		if (Input.GetButtonUp(this.m_ActionInputKey))
		{
			if(this.m_GrabbedItem != null)
			{
                // Give item
                if(m_ClientDetection.currentlyAvailableClient != null)
                {
                    GiveItem();
                }
                else
                {
                    //Throw item !
                    ThrowItem();
                }

			} 
			else 
			{
				//Grab item !
				GrabItem(m_ItemDetection.currentlyAvailableItem);
			}
		}
	}

	//---- Grab and throw ----//
    private void GiveItem()
    {
        m_ClientDetection.currentlyAvailableClient.InteractWithPlayer(this.m_PlayerComponent);
        m_ItemDetection.setActivateHighlightOnSelect(true);
        m_ClientDetection.setActivateHighlightOnSelect(false);
        m_ActionBillboard.SetActive(false);
    }

	private void GrabItem(Item itemToGrab)
	{
		if(itemToGrab == null || this.m_GrabbedItem != null)
			return;
		
		// Store item
		m_GrabbedItem = m_ItemDetection.currentlyAvailableItem;
		if(m_PlayerComponent != null)
			m_PlayerComponent.currentItem = m_GrabbedItem;
        
        m_ActionBillboard.SetActive(false);
        m_ItemDetection.setActivateHighlightOnSelect(false);
        m_ClientDetection.setActivateHighlightOnSelect(true); // Ready for clients

        // Grab item
        if (this.m_GrabbingPoint != null)
		{	
			m_GrabbedItem.transform.parent = this.m_GrabbingPoint.transform;
			m_GrabbedItem.transform.localPosition = Vector3.zero;

			// Freeze physics
			m_GrabbedItem.GetComponent<Collider>().enabled = false;
			m_GrabbedItem.GetComponent<Rigidbody>().isKinematic = true;
		}
	}

	private void ThrowItem()
	{
		if(this.m_GrabbedItem == null)
			return;

		// Detach
		m_GrabbedItem.transform.parent = null;

		// Unfreeze physics
		m_GrabbedItem.GetComponent<Collider>().enabled = true;
		m_GrabbedItem.GetComponent<Rigidbody>().isKinematic = false;

        // Throw
		m_GrabbedItem.GetComponent<Rigidbody>().AddForce(this.transform.rotation*throwingVector, ForceMode.Impulse);

		m_GrabbedItem = null;
		m_PlayerComponent.currentItem = null;
        m_ItemDetection.setActivateHighlightOnSelect(true);
        m_ClientDetection.setActivateHighlightOnSelect(false);
    }
	//---- Grab and throw ----//

	// Fixed update is called in sync with physics
	private void FixedUpdate()
	{
		// read inputs
		float h = Input.GetAxis(m_HorizontalAxisName);
		float v = Input.GetAxis(m_VerticalAxisName);

		// we use world-relative directions in the case of no main camera
		Vector3 moveInput = v*Vector3.forward + h*Vector3.right;

		// Move the character
		m_HumanBody.Move(moveInput);

	}

	//---- ItemDetectionListener ----//
	public void NewAvailableItem(Item newItem)
	{
		//TODO: show billboard
		if(this.m_GrabbedItem == null)
			m_ActionBillboard.SetActive(true);
	}

    public void NoItemCurrentlyAvailable(Item newItem)
    {
        if (this.m_GrabbedItem == null)
        {
            //TODO: hide billboard
            m_ActionBillboard.SetActive(false);
        }
    }

    public void NewAvailableItem(Client newItem)
    {
        // New client detected
        if (this.m_GrabbedItem != null)
            m_ActionBillboard.SetActive(true); // show billboard if the player is holding an item
    }
    public void NoItemCurrentlyAvailable(Client newItem)
    {
        if (this.m_GrabbedItem != null)
        {
            //TODO: hide billboard
            m_ActionBillboard.SetActive(false);
        }
    }
    //---- ItemDetectionListener ----//
}
