﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerToClientDetection : MonoBehaviour
{

    public string ItemTag = "Client";

    public Client currentlyAvailableClient = null;
    public List<Client> availableClients;

    private List<ItemDetectionListener<Client>> m_Listeners;

    private bool activateHighlightOnSelect = false;
    public void setActivateHighlightOnSelect(bool activate)
    {
        activateHighlightOnSelect = activate;
        if(currentlyAvailableClient != null)
        {
            if (activateHighlightOnSelect)
                currentlyAvailableClient.Select();
            else
                currentlyAvailableClient.Deselect();
        }
    }


    //------------------- Listener -------------------//
    public void RegisterListener(ItemDetectionListener<Client> newListener)
    {
        if (m_Listeners == null)
            m_Listeners = new List<ItemDetectionListener<Client>>();
        m_Listeners.Add(newListener);
    }
    public void UnregisterListener(ItemDetectionListener<Client> newListener)
    {
        if (m_Listeners == null)
            m_Listeners = new List<ItemDetectionListener<Client>>();
        m_Listeners.Remove(newListener);
    }

    private void SendNewItemToListeners(Client newItem)
    {
        if (this.m_Listeners == null)
            return;

        List<int> deleteIndexList = new List<int>();

        for (int i = 0; i < this.m_Listeners.Count; i++)
        {
            if (this.m_Listeners[i] == null)
            {
                deleteIndexList.Add(i);
            }
            else
            {
                this.m_Listeners[i].NewAvailableItem(newItem);
            }
        }

        // Remove missing items stuff
        for (int i = 0; i < deleteIndexList.Count; i++)
        {
            this.m_Listeners.RemoveAt(i);
        }
    }

    private void SendNoItemCurrentlyAvailableToListener(Client oldItem)
    {
        if (this.m_Listeners == null)
            return;

        List<int> deleteIndexList = new List<int>();

        for (int i = 0; i < this.m_Listeners.Count; i++)
        {
            if (this.m_Listeners[i] == null)
            {
                deleteIndexList.Add(i);
            }
            else
            {
                this.m_Listeners[i].NoItemCurrentlyAvailable(oldItem);
            }
        }

        // Remove missing items stuff
        for (int i = 0; i < deleteIndexList.Count; i++)
        {
            this.m_Listeners.RemoveAt(i);
        }
    }
    //------------------- Listener -------------------//

    // Update is called once per frame
    void Update()
    {
        ProcessAvailableItems();
    }

    void ProcessAvailableItems()
    {
        if (availableClients != null && availableClients.Count > 0)
        {
            KeyValuePair<int, float> closestItem = new KeyValuePair<int, float>(-1, Mathf.Infinity);
            if (currentlyAvailableClient != null)
                closestItem = new KeyValuePair<int, float>(-1, Vector3.Distance(transform.position, currentlyAvailableClient.transform.position)); // We don't care for object closer than this

            List<int> deleteIndexList = new List<int>();

            // TODO: iterate on available item list, check the closest one and displayActionPrompt on it
            for (int i = 0; i < this.availableClients.Count; i++)
            {
                if (this.availableClients[i] == null)
                {
                    deleteIndexList.Add(i);
                }
                else
                {
                    float distanceToObject = Vector3.Distance(transform.position, this.availableClients[i].transform.position);
                    if (closestItem.Value > distanceToObject)
                    {
                        closestItem = new KeyValuePair<int, float>(i, distanceToObject);
                    }
                }
            }

            // Remove missing items stuff
            for (int i = 0; i < deleteIndexList.Count; i++)
            {
                this.availableClients.RemoveAt(i);
            }

            // Do We have something usefull ?
            SetCurrentlyAvailableItem(closestItem.Key);
        }
    }

    void SetCurrentlyAvailableItem(int itemIndex)
    {
        // Do We have something usefull ?
        if (itemIndex != -1)
        {
            // ACTUAL ACTION UP IN HERE
            Client newItem = this.availableClients[itemIndex];
            if (currentlyAvailableClient != null && activateHighlightOnSelect)
            {
                currentlyAvailableClient.Deselect();
            }

            currentlyAvailableClient = newItem;
            if(activateHighlightOnSelect)
                currentlyAvailableClient.Select();
            SendNewItemToListeners(currentlyAvailableClient);
        }
    }

    void ResetCurrentlyAvailable()
    {
        if (currentlyAvailableClient != null)
        {
            if (activateHighlightOnSelect)
                currentlyAvailableClient.Deselect();
            SendNoItemCurrentlyAvailableToListener(currentlyAvailableClient);
            currentlyAvailableClient = null;
        }
    }


    //------------------- Trigger -------------------//
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(ItemTag))
        {
            Client item = other.gameObject.GetComponent<Client>();
            if (item != null)
            {
                // TODO Should we check something on item ?
                if (!availableClients.Contains(item))
                {
                    availableClients.Add(item);
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(ItemTag))
        {
            Client item = other.gameObject.GetComponent<Client>();
            if (item != null)
            {
                if (this.currentlyAvailableClient == item)
                    ResetCurrentlyAvailable();

                // TODO Should we check something on item ?
                if (availableClients.Contains(item))
                {
                    availableClients.Remove(item);
                }
            }
        }
    }

}
