﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDetection : MonoBehaviour {

	public string ItemTag = "Item";

	public Item currentlyAvailableItem = null;
	public List<Item> availableItems;

	private List<ItemDetectionListener<Item>> m_Listeners;

    private bool activateHighlightOnSelect = false;
    public void setActivateHighlightOnSelect(bool activate)
    {
        activateHighlightOnSelect = activate;
        if (currentlyAvailableItem != null)
        {
            if (activateHighlightOnSelect)
                currentlyAvailableItem.Select();
            else
                currentlyAvailableItem.Deselect();
        }
    }

    //------------------- Listener -------------------//
    public void RegisterListener(ItemDetectionListener<Item> newListener)
	{
		if(m_Listeners == null)
			m_Listeners = new List<ItemDetectionListener<Item>>();
		m_Listeners.Add(newListener);
	}
	public void UnregisterListener(ItemDetectionListener<Item> newListener)
	{
		if(m_Listeners == null)
			m_Listeners = new List<ItemDetectionListener<Item>>();
		m_Listeners.Remove(newListener);
	}

	private void SendNewItemToListeners(Item newItem)
	{
		if(this.m_Listeners == null)
			return;
		
		List<int> deleteIndexList = new List<int>();

		for(int i = 0; i < this.m_Listeners.Count; i++)
		{
			if(this.m_Listeners[i] == null)
			{
				deleteIndexList.Add(i);
			}
			else {
				this.m_Listeners[i].NewAvailableItem(newItem);
			}
		}

		// Remove missing items stuff
		for(int i = 0; i < deleteIndexList.Count; i++)
		{
			this.m_Listeners.RemoveAt(i);
		}
	}

	private void SendNoItemCurrentlyAvailableToListener(Item oldItem)
	{
		if(this.m_Listeners == null)
			return;
		
		List<int> deleteIndexList = new List<int>();

		for(int i = 0; i < this.m_Listeners.Count; i++)
		{
			if(this.m_Listeners[i] == null)
			{
				deleteIndexList.Add(i);
			}
			else {
				this.m_Listeners[i].NoItemCurrentlyAvailable(oldItem);
			}
		}

		// Remove missing items stuff
		for(int i = 0; i < deleteIndexList.Count; i++)
		{
			this.m_Listeners.RemoveAt(i);
		}
	}
	//------------------- Listener -------------------//

	// Update is called once per frame
	void Update () {
		ProcessAvailableItems();
	}

	void ProcessAvailableItems()
	{
		if(availableItems != null && availableItems.Count > 0)
		{
			KeyValuePair<int, float> closestItem = new KeyValuePair<int, float>(-1, Mathf.Infinity);
			if(currentlyAvailableItem != null)
				closestItem = new KeyValuePair<int, float>(-1,Vector3.Distance( transform.position, currentlyAvailableItem.transform.position)); // We don't care for object closer than this

			List<int> deleteIndexList = new List<int>();

			// TODO: iterate on available item list, check the closest one and displayActionPrompt on it
			for(int i = 0; i < this.availableItems.Count; i++)
			{
				if(this.availableItems[i] == null)
				{
					deleteIndexList.Add(i);
				}
				else {
					float distanceToObject = Vector3.Distance( transform.position, this.availableItems[i].transform.position);
					if(closestItem.Value > distanceToObject)
					{
						closestItem = new KeyValuePair<int, float>(i, distanceToObject);
					}
				}
			}

			// Remove missing items stuff
			for(int i = 0; i < deleteIndexList.Count; i++)
			{
				this.availableItems.RemoveAt(i);
			}

			// Do We have something usefull ?
			SetCurrentlyAvailableItem(closestItem.Key);
		}
	}

	void SetCurrentlyAvailableItem(int itemIndex)
	{
		// Do We have something usefull ?
		if(itemIndex != -1)
		{
			// ACTUAL ACTION UP IN HERE
			Item newItem = this.availableItems[itemIndex];
			if(currentlyAvailableItem != null && activateHighlightOnSelect)
			{
				currentlyAvailableItem.Deselect();
			}

			currentlyAvailableItem = newItem;
            if (activateHighlightOnSelect)
                currentlyAvailableItem.Select();
			SendNewItemToListeners(currentlyAvailableItem);
		}
	}

	void ResetCurrentlyAvailable()
	{
		if(currentlyAvailableItem != null)
        {
            if (activateHighlightOnSelect)
                currentlyAvailableItem.Deselect();
            SendNoItemCurrentlyAvailableToListener(currentlyAvailableItem);
            currentlyAvailableItem = null;
		}
	}


	//------------------- Trigger -------------------//
	void OnTriggerEnter(Collider other) {
		if(other.CompareTag(ItemTag))
		{
			Item item = other.gameObject.GetComponent<Item>();
			if(item != null)
			{
				// TODO Should we check something on item ?
				if(!availableItems.Contains(item))
				{
					availableItems.Add(item);
				}
			}
		}
	}

	void OnTriggerExit(Collider other) {
		if(other.CompareTag(ItemTag))
		{
			Item item = other.gameObject.GetComponent<Item>();
			if(item != null)
			{
				if(this.currentlyAvailableItem == item)
					ResetCurrentlyAvailable();
				
				// TODO Should we check something on item ?
				if(availableItems.Contains(item))
				{
					availableItems.Remove(item);
				}
			}
		}
	}

}
