﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	//Load scene
	public void CallLoadNewGame()
	{
		MenuManager.LoadNewGame();
	}
	public static void LoadNewGame()
	{
		SceneManager.LoadScene("TestSceneTibo");
	}

	public void CallLoadHowTo()
	{
		MenuManager.LoadHowTo();
	}
	public static void LoadHowTo()
	{
		SceneManager.LoadScene("HowTo");
	}

	public void CallLoadCredits()
	{
		MenuManager.LoadCredits();
	}
	public static void LoadCredits()
	{
		SceneManager.LoadScene("Credits");
	}

	
	public void CallLoadTitleScreen()
	{
		MenuManager.TitleScreen();
	}
	public static void TitleScreen()
	{
		SceneManager.LoadScene("TitleScreen");
	}
	

	// Quit
	public void CallQuitToDesktop()
	{
		MenuManager.QuitToDesktop();
	}
	public static void QuitToDesktop()
	{
		Application.Quit();
	}
}
